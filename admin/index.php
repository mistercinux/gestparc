<?php
/**
 * Created by PhpStorm.
 * User: cjaming
 * Date: 26/04/2018
 * Time: 13:45
 */

if (empty($_SESSION))
    session_start();

// On vérifie que l'user a le droit d'afficher cette page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header('location: /');
    die();
}

// Inclusion des fichiers
include_once ($_SERVER['DOCUMENT_ROOT'].'/admin/includes/pages.inc.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/admin/models/Ldap.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/admin/models/dbAccess.php');


// On vérifie si la page demandée existe
$requestedPage = '';
if (!empty($_GET['page'])) {
    foreach ($page as $item => $value) {
        if ($_GET['page'] === $item)
            $requestedPage = $value['file'];
    }
    if ($requestedPage === '')
        $requestedPage = $page404;
}
else
    $requestedPage = $page['home']['file'];
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <title>Administration</title>
</head>
<body class="bg-light">
<div class="container bg-light">
    <div class="row bg-light"  style="background-color: aliceblue">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">

            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <?php
                    foreach ($page as $item => $value) {

                        if ($item === 'home')
                            echo '<a class="navbar-brand" href="./index.php?page=' . $item . '">' . $value['titre'] . '</a>';
                        else if ($page[$item]['file'] === $requestedPage)
                            echo '<a class="nav-item nav-link active" href="./index.php?page=' . $item . '">' . $value['titre'] . '</a>';
                        else
                            echo '<a class="nav-item nav-link" href="./index.php?page=' . $item . '">' . $value['titre'] . '</a>';
                    }
                    ?>
                    <a class="nav-item nav-link" href="/admin/adminer.php?username=gestparc&db=gestparc">SQL Adminer</a>
                    <a class="nav-item nav-link" href="/logout.php">Déconnexion</a>
                </div>
            </div>
        </nav>
    </div>
    <div class="row" style="width: 100%">
        <div style="padding: 30px; width: 100%;">
            <?php
            if (!include($requestedPage))
                include ($page404);
            ?>
        </div>
    </div>
</div>

<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/gestparc.js"></script>
