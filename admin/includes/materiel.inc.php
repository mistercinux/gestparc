<?php
/**
 * Created by PhpStorm.
 * User: cjaming
 * Date: 26/04/2018
 * Time: 16:56
 */
?>

<?php

// On récupère les types de matériel pour les injecter dans les options ci-dessous
$dbAccess = new dbAccess();
$listeTypes = $dbAccess->getTypesMaterielAsArray();
$listeModels = $dbAccess->getModelsAsArray();
$listeMarques = $dbAccess->getMarquesAsArray();
$listeUtilisateurs = $dbAccess->getUsersAsArray();
$listeServices = $dbAccess->getServicesAsArray();


if (isset($_GET['action']) && $_GET['action'] === 'addMateriel') {
    if (!empty($_POST) && !empty($_POST['id_model']) && !empty($_POST['id_utilisateur']) && !empty($_POST['date_achat'])) {
        if (!$dbAccess->addMateriel($_POST['id_model'], $_POST['id_utilisateur'], $_POST['localisation'], $_POST['date_achat'])) {
            echo $dbAccess->getError();
        }
    }
}

if ($listeTypes) {
    echo '<select class="custom-select" name="type" id="type" onchange=afficherTableauMateriel()>';
    echo "<option value=''>Tous types</option>";
    // On affiche les options de la liste déroulante
    foreach ($listeTypes as $type) {
        echo "<option value='" . $type['id'] . "'>" . $type['nom_type'] . "</option>";
    }
}


if ($listeUtilisateurs) {
    echo '</select>';
    echo '<select class="custom-select" name="utilisateurs" id="utilisateurs" onchange=afficherTableauMateriel()>';
    echo "<option value=''>Touts les utilisateurs </option>";
    // On affiche les options de la liste déroulante
    foreach ($listeUtilisateurs as $utilisateur) {
        echo "<option value='". $utilisateur['id']."'>".$utilisateur['nom_prenom']."</option>";
    }
    echo '</select>';
}


// Le tableau sera injecté dans la div ci-dessous lors de la selection avec la liste déroulante
//   via une requête ajax dans gestparc.js.
?>


<div id="divTableauMateriel">
    <?php
    include_once ($_SERVER['DOCUMENT_ROOT'].'/admin/ajax/afficherTableauMateriel.php');
    ?>
</div>

