<?php
/**
 * Created by PhpStorm.
 * User: cjaming
 * Date: 26/04/2018
 * Time: 16:56
 */
?>

<?php
$dbAccess = new dbAccess();

if (!empty($_GET) && !empty($_GET['action'])){
    if ($_GET['action'] === 'addModel') {
        if (!$dbAccess->addModel($_POST['type'], $_POST['marque'], $_POST['model'])) {
            $message = '<p style="color:red">Une erreur est survenue lors de l\'ajout du model : '.$dbAccess->getError().'</p>';
        }
    }
}

// On récupère les types de matériel pour les injecter dans les options ci-dessous
$listeTypes = $dbAccess->getTypesMaterielAsArray();
$listeMarques = $dbAccess->getMarquesAsArray();
$listeUtilisateurs = $dbAccess->getUsersAsArray();

if ($listeTypes) {
    echo '<select class="custom-select" name="type" id="type" onchange=afficherTableauModels()>';
    echo "<option value=''>Tous types</option>";
    // On affiche les options de la liste déroulante
    foreach ($listeTypes as $type) {
        echo "<option value='" . $type['id'] . "'>" . $type['nom_type'] . "</option>";
    }
}

if ($listeMarques) {
    echo '</select>';
    echo '<select class="custom-select" name="marque" id="marque" onchange=afficherTableauModels()>';
    echo "<option value=''>Toutes marques</option>";
    // On affiche les options de la liste déroulante
    foreach ($listeMarques as $marque) {
        echo "<option value='". $marque['id']."'>".$marque['nom_marque']."</option>";
    }
    echo '</select>';
}


// Le tableau sera injecté dans la div ci-dessous lors de la selection avec la liste déroulante
//   via une requête ajax dans gestparc.js.

?>

 ou <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addModel">Ajouter un model de materiel</button><br />

<?php
echo $message;
?>

<div id="divTableauModels">
    <?php
    include_once ($_SERVER['DOCUMENT_ROOT'].'/admin/ajax/afficherTableauModels.php');
    ?>
</div>



