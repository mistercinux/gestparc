<?php
/**
 * Created by PhpStorm.
 * User: cjaming
 * Date: 26/04/2018
 * Time: 16:58
 */


if (!empty($_GET) && isset($_GET['action']) && !empty($_GET['action']) && $_GET['action'] === 'updateDB') {

    echo 'Mise à jour des utilisateurs dans la base de donnée en cours... <br />';
    $servicesDn = 'ou=Utilisateurs, ou=Site Strasbourg, ou=AGDLP, dc=toto, dc=lan'; // dn des services
    $filter = '(ou=*)';

    $ldap = new Ldap();
    $dbaccess = new dbAccess();

// Mise à jour des utilisateurs dans la base de données
    if ($ldap->getConState()) {
        if ($ldap->bind()) {
            $items = $ldap->getUsersInServices();
            if ($items) {
                foreach ($items as $service => $value) {
                    foreach ($value['utilisateurs'] as $utilisateur => $dn) {
                        if (!$dbaccess->getUser($dn)) {
                            if (!$dbaccess->addUser($utilisateur, $dn, $service))
                                $message = '<p style="color:red">Erreur de création de l\'utilisateur : ' . $dbaccess->getError() . '</p>';
                        } else {
                            if (!$dbaccess->updateUser($utilisateur, $dn, $service))
                                $message = '<p style="color:red">Erreur mise à jour des informations de l\'utilisateur : ' . $dbaccess->getError() . '</p>';
                        }
                    }
                }
            }
            echo '<p style="color:green">La table "utilisateurs" mise à jour avec succès.</p>';
        } else
        echo '<br />Erreur d\'authentification';
    } else
        echo '<br />Impossible de se connecter au serveur Active Directory';

}

echo $message;

?>


<?php
$dbAccess = new dbAccess();
$listeServices = $dbAccess->getServicesAsArray();

if ($listeServices) {
    echo '<select class="custom-select" name="id_service" id="id_service" onchange=afficherTableauUtilisateurs()>';
    echo "<option value=''>Tous les services</option>";
    // On affiche les options de la liste déroulante
    foreach ($listeServices as $service) {
        echo "<option value='" . $service['id'] . "'>" . $service['nom_service'] . "</option>";
    }
    echo '</select>';
}
?>

<a class="btn btn-info" href="/admin/index.php?page=utilisateurs&action=updateDB">Mettre à jour la base utilisateurs</a>
<div id="divTableauUtilisateurs">
    <?php
    include_once ($_SERVER['DOCUMENT_ROOT'].'/admin/ajax/afficherTableauUtilisateurs.php');
    ?>
</div>
