<?php
/**
 * Created by PhpStorm.
 * User: cjaming
 * Date: 27/09/2017
 * Time: 09:55
 *
 * Liste des pages disponnibles aux clients et aux admins
 * Cela empêchera l'inclusion de fichier distants
 *
 */

$page = array(
    'home' => array('file' => $_SERVER['DOCUMENT_ROOT'].'/admin/includes//home.inc.php',
        'titre' => 'Accueil'),
    'utilisateurs' => array('file' => $_SERVER['DOCUMENT_ROOT'].'/admin/includes/utilisateurs.inc.php',
        'titre' => 'Afficher les utilisateurs'),
    'materiel' => array('file' => $_SERVER['DOCUMENT_ROOT'].'/admin/includes/materiel.inc.php',
        'titre' => 'Inventaire parc'),
    'models' => array('file' => $_SERVER['DOCUMENT_ROOT'].'/admin/includes/models.inc.php',
        'titre' => 'Gérer les modeles')
    //'contact' => array('file' => $_SERVER['DOCUMENT_ROOT'].'/pages/contact.php',
    //    'titre' => 'Nous contacter')
);

$page404 = '/pages/404.php';
