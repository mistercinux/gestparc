<?php

include_once ($_SERVER['DOCUMENT_ROOT'].'/admin/models/dbAccess.php');

if (isset($_POST)) {
    $dbAccess2 = new dbAccess();
    if (!isset($_POST['service'])) {
        $_POST['service'] = '';
    }

    $res = $dbAccess2->getProperUsersAsArray($_POST['service']);

    // On affiche le resultat
    echo '<br /><br /><table class=\'table table-sm table-light table-bordered table-hover\'>';

    echo '<thead><tr  style=\'background-color: #c8e6c9\'><th>id</th><th>Utilisateur</th><th>Service</th></tr>';
    foreach ($res as $row) {
        echo '<tr>';
        echo '<td>' . $row['id'] . '</td><td>' . $row['nom_prenom'] . '</td><td>' . $row['nom_service']. '</td></tr>';
    }
    echo '</table>';

}
else
    echo 'toto';
