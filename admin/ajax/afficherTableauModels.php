<?php

include_once ($_SERVER['DOCUMENT_ROOT'].'/admin/models/dbAccess.php');
$dbAccess = new dbAccess();

if (isset($_POST) && isset($_POST['type']) && isset($_POST['marque'])) {
    // On récupère la liste du materiel
    $res = $dbAccess->getModelsAsArray($_POST['type'], $_POST['marque']);
}
else
    $res = $dbAccess->getModelsAsArray();

//if (isset($_GET['action']) && $_GET['action'] === 'addMateriel') {
//
//}

// On récupère les types de matériel pour les injecter dans les options ci-dessous
$listeTypes = $dbAccess->getTypesMaterielAsArray();
$listeMarques = $dbAccess->getMarquesAsArray();
$listeUtilisateurs = $dbAccess->getUsersAsArray();

// On affiche le resultat
echo '<br /><br /><table class=\'table table-sm table-light table-bordered table-hover\'>';
echo '<thead><tr  style=\'background-color: #c8e6c9\'><th>id</th><th>Model</th><th>Marque</th><th>type</th><th style="width: 50px">Action</th></tr></thead>';
foreach ($res as $item) {
    echo '<tr><td>' . $item['id'] . '</td><td>' . $item['model'] . '</td><td>' . $item['nom_marque'] . '</td><td>' . $item['nom_type'] . '</td><td id="'.$item['id'].'" style="margin: 10px"><button class="btn btn-link btn-ajoutParc" data-toggle="modal" data-target="#addMateriel" style="margin: 0px; padding: 0px">Ajouter au parc</button></td></tr>';
}
echo '</table>';


?>

<div class="modal fade" id="addModel" tabindex="-1" role="dialog" aria-labelledby="addMateriel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addMateriel">Ajouter un materiel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="addModelBody">
                <form action="/admin/index.php?page=models&action=addModel" method="post" style="width: auto; margin-left: auto; margin-right: auto">
                    <tmp id="message"></tmp>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="active" class="col-form-label">Type:</label>
                            <select class="form-control" name="type" id="selectType">
                                <option value="">---</option>
                                <?php
                                foreach ($listeTypes as $type) {
                                    echo "<option value='".$type['id']."'>".$type['nom_type']."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="marque" class="col-form-label">Marque:</label>
                            <select class="form-control" name="marque" id="marque">
                                <option value="">---</option>
                                <?php
                                foreach ($listeMarques as $marques) {
                                    echo "<option value='".$marques['id']."'>".$marques['nom_marque']."</option>";
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="model" class="col-form-label">Modele :</label>
                            <input class="form-control" type="text" name="model" placeholder="Ex : LPB2800"/>
                        </div>
                    </div>
                    <button type="submit" name="send" class="btn btn-info" id="enregistrer">Enregistrer</button>
                    <input type="hidden" name="action" value="addProduct">
                </form>
            </div>
        </div>
    </div>
</div>

