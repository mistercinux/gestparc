<?php

include_once ($_SERVER['DOCUMENT_ROOT'].'/admin/models/dbAccess.php');

if (isset($_POST)) {
    // On récupère la liste du materiel
    $dbAccess = new dbAccess();
    $res = $dbAccess->getMaterielAsArray($_POST['type'], $_POST['utilisateur']);

    // On affiche le resultat
    echo '<br /><br /><table class=\'table table-sm table-light table-bordered table-hover\'>';

    echo '<thead><tr  style=\'background-color: #c8e6c9\'><th>id</th><th>Type</th><th>Localisation</th><th>Model</th><th>Marque</th><th>Utilisateur</th><th>Date d\'achat</th><th>Service</th></tr>';
    foreach ($res as $row) {
        echo '<tr>';
        echo '<td>' . $row['id'] . '</td><td>' . $row['nom_type'] . '</td><td>' . $row['localisation'] . '</td><td>' . $row['model'] . '</td><td>' . $row['nom_marque'] . '</td><td>' . $row['utilisateur'] . '</td><td>' . $row['date_achat'] . '</td><td>' . $row['nom_service'] . '</td></tr>';
    }
    echo '</table>';

}
