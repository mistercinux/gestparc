<?php
/**
 * Created by PhpStorm.
 * User: cinux
 * Date: 15/05/18
 * Time: 13:20
 */


class dbAccess {

    // db connexion informations
    protected $host                 = 'localhost';            // Nom du serveur sql
    protected $dbName               = 'gestparc';             // Nom de la base sql à créer/utiliser
    protected $username             = 'gestparc';             // Nom d'utilisateur d'administration de la base sql
    protected $password             = 'Adm.2018';             // Mot de pass de l'utilisateur sql
    protected $modelsTable          = 'models';
    protected $marquesTable         = 'marques';
    protected $typeTable            = 'type_materiel';
    protected $materielTable        = 'materiel';
    protected $utilisateursTable    = 'utilisateurs';
    protected $servicesTable        = 'services';
    protected $error                = '';                     // Variable de récupération d'erreur

    protected $mysqli;


    /**
     * Fonction de récupération de la liste de materiel sous forme de tableau
     * @param $type
     *      Le type de materiel doit figurer dans la table type_materiel.
     *      Si le type n'existe pas dans la table type_materiel l'argument sera remplacé par la valeur 'all'.
     * @return bool|array
     */
    public function getMaterielAsArray($type = '', $utilisateur = '') {

        $type = filter_var($type, FILTER_SANITIZE_NUMBER_INT);
        $utilisateur = filter_var($utilisateur, FILTER_SANITIZE_NUMBER_INT);

        if (empty($type) && empty($utilisateur)) {
            $where = '';
        }
        elseif (empty($type)) {
            $where = " WHERE id_utilisateur = $utilisateur";
        }
        elseif (empty($utilisateur)) {
            $where = " WHERE id_type = $type";
        }
        else {
            $where = " WHERE id_type=$type AND id_utilisateur=$utilisateur";
        }

        $query = "SELECT * FROM vue_materiel_utilisateurs_id ".$where;
        $res = $this->mysqli->query($query);

        //echo "$query <br />";

        $array = array();
        while ($row = $res->fetch_assoc()){
            $array[] = $row;
        }

        if (empty($array))
            return false;
        else
            return $array;
    }
    /**
     * Fonction de récupération d'un utilisateur (utilisateur, dn, service)
     * @param $dn
     * @return array|bool
     */
    public function getUser($dn) {

        $dn = filter_var($dn, FILTER_SANITIZE_MAGIC_QUOTES);

        $query = "SELECT * FROM utilisateurs";
        $res = $this->mysqli->query($query);
        while ( $row = $res->fetch_assoc() ) {
            if ($row['id_ad'] === $dn)
                return $row;
        }
        return false;
    }

    public function updateUser ($nom, $dn, $service) {
        $nom = filter_var($nom, FILTER_SANITIZE_MAGIC_QUOTES);
        $dn = filter_var($dn, FILTER_SANITIZE_MAGIC_QUOTES);

        // On vérifie que l'utilisateur existe
        $res = $this->mysqli->query("SELECT id FROM utilisateurs WHERE id_ad LIKE '%$dn%'");
        $row = $res->fetch_assoc();

        if (!$row) {
            $this->error = "L'utilisateur n'existe pas.";
            return false;
        }

        // récupération de l'id du service en question. Si il n'est pas trouvé, on ajoute l'utilisateur au service 'autre'
        $res = $this->mysqli->query("SELECT id FROM services WHERE nom_service LIKE '%$service%'");
        if ($row = $res->fetch_assoc())
            $id_service = $row['id'];
        else {
            $res = $this->mysqli->query("SELECT id FROM services WHERE nom_service='autre'");
            $row = $res->fetch_assoc();
        }

        // Update des informations de l'utilisateur
        $query = "UPDATE utilisateurs SET nom_prenom='$nom', id_service=$id_service WHERE id_ad='$dn'";
        if (!$this->mysqli->query($query)) {
            $this->error = "Erreur lors de la requête : $query<br />".$this->mysqli->error;
            return false;
        }
        return true;
    }


    public function addUser ($nom, $dn, $service) {
        $nom = filter_var($nom, FILTER_SANITIZE_MAGIC_QUOTES);
        $dn = filter_var($dn, FILTER_SANITIZE_MAGIC_QUOTES);

        // récupération de l'id du service en question. Si il n'est pas trouvé, on ajoute l'utilisateur au service 'autre'
        $res = $this->mysqli->query("SELECT id FROM services WHERE nom_service LIKE '%$service%'");
        if ($row = $res->fetch_assoc())
            $id_service = $row['id'];
        else {
            $res = $this->mysqli->query("SELECT id FROM services WHERE nom_service='autre'");
            $row = $res->fetch_assoc();
        }

        // On vérifie que l'utilisateur existe déjà
        $res = $this->mysqli->query("SELECT id FROM utilisateurs WHERE id_ad LIKE '%$dn%'");
        $row = $res->fetch_assoc();
        if ($row) {
            $this->error = "L'utilisateur existe déjà.";
            return false;
        }

        // Création de l'utilisateur
        $query = "INSERT INTO utilisateurs (nom_prenom, id_ad, id_service) VALUES ('$nom', '$dn', $id_service)";
        if (!$this->mysqli->query($query)) {
            $this->error = "Erreur lors de la requête : $query<br />".$this->mysqli->error;
            return false;
        }
        return true;
    }

    /**
     * Fonction de récupération du type de materiel sous forme de tableau à 2 dimensions
     * @return array|bool
     */
    public function getTypesMaterielAsArray() {
        $res = $this->mysqli->query("SELECT * FROM $this->typeTable ORDER BY nom_type");
        $array = array();
        while ($row = $res->fetch_assoc()){
            $array[] = $row;

        }
        if (empty($array))
            return false;
        else
            return $array;
    }

    /**
     * Fonction de récupération des marques sous forme de tableaux à 2 dimensions
     * @return array|bool
     */
    public function getMarquesAsArray() {
        $res = $this->mysqli->query("SELECT * FROM $this->marquesTable ORDER BY nom_marque");
        $array = array();
        while ($row = $res->fetch_assoc()){
            $array[] = $row;
        }
        if (empty($array))
            return false;
        else
            return $array;
    }


    /**
     * Fonction de récupération des models sous forme de tableaux à 2 dimensions
     * @return array|bool
     */
    public function getModelsAsArray($type = '', $marque = '') {

        $type = filter_var($type, FILTER_SANITIZE_NUMBER_INT);
        $marque = filter_var($marque, FILTER_SANITIZE_NUMBER_INT);

        if (empty($type) && empty($marque)) {
            $where = '';
        }
        elseif (empty($type)) {
            $where = " WHERE id_marque = $marque";
        }
        elseif (empty($marque)) {
            $where = " WHERE id_type = $type";
        }
        else {
            $where = " WHERE id_type=$type AND id_marque=$marque";
        }

        $query = "SELECT * FROM vue_models_id ".$where. " ORDER BY model";
        $res = $this->mysqli->query($query);

        //echo "$query <br />";

        $array = array();
        while ($row = $res->fetch_assoc()){
            $array[] = $row;
        }

        if (empty($array))
            return false;
        else
            return $array;
    }


    /**
     * Fonction de récupération des utilisateurs
     * @return array|bool
     */
    public function getUsersAsArray($service='') {

        $service = filter_var($service, FILTER_SANITIZE_NUMBER_INT);
        if (!empty($service))
            $where = ' WHERE id_service=$service ';
        else
            $where = '';

        $res = $this->mysqli->query("SELECT * FROM $this->utilisateursTable $where ORDER BY nom_prenom");
        $array = array();
        while ($row = $res->fetch_assoc()){
            $array[] = $row;
        }
        if (empty($array))
            return false;
        else
            return $array;
    }

    /**
     * Fonction de récupération des utilisateurs sans les services
     * @return array|bool
     */
    public function getProperUsersAsArray($service='') {

        $service = filter_var($service, FILTER_SANITIZE_NUMBER_INT);
        if (!empty($service))
            $where = " WHERE id_service=$service ";
        else
            $where = '';

        $res = $this->mysqli->query("SELECT * FROM vue_utilisateurs $where ORDER BY nom_prenom");
        $array = array();
        while ($row = $res->fetch_assoc()){
            $array[] = $row;
        }

        if (empty($array))
            return false;
        else
            return $array;
    }


    /**
     * Fonction de récupération des services
     * @return array|bool
     */
    public function getServicesAsArray() {
        $res = $this->mysqli->query("SELECT * FROM $this->servicesTable");
        $array = array();
        while ($row = $res->fetch_assoc()){
            $array[] = $row;
        }
        if (empty($array))
            return false;
        else
            return $array;
    }

    /**
     * Fonction d'ajout d'un model dans la base de données
     * @param $id_type
     * @param $id_marque
     * @param $model
     * @return bool
     */
    public function addModel($id_type, $id_marque, $model) {

        $id_type = filter_var($id_type, FILTER_SANITIZE_NUMBER_INT);
        $id_marque = filter_var($id_marque, FILTER_SANITIZE_NUMBER_INT);
        $model = filter_var($model, FILTER_SANITIZE_MAGIC_QUOTES);

        // On vérifie si le model n'est pas encore enregistré
        $query = "SELECT id FROM models WHERE model LIKE '$model'";
        $res = $this->mysqli->query($query);
        $row = $res->fetch_assoc();
        if ($row) {
            $this->error = "Le modèle existe déjà dans la base de données";
            return false;
        }

        $query = "INSERT INTO models (id_type, id_marque, model) VALUES ('$id_type', '$id_marque', '$model')";

        if (!$this->mysqli->query($query)) {
            $this->error = "L'objet n'a pas pu être ajouté à la base de données. ".$this->mysqli->error;
            return false;
        }
        else
            return true;

    }


    /**
     * Fonction d'attribution de materiel à un utilisateur
     * @param $id_model
     * @param $id_utilisateur
     * @param $localisation
     * @param $date_achat
     * @return bool
     */
    public function addMateriel ($id_model, $id_utilisateur, $localisation, $date_achat) {
        $id_model = filter_var($id_model, FILTER_SANITIZE_NUMBER_INT);
        $id_utilisateur = filter_var($id_utilisateur, FILTER_SANITIZE_NUMBER_INT);
        $localisation = filter_var($localisation, FILTER_SANITIZE_MAGIC_QUOTES);
        $date_achat = filter_var($date_achat, FILTER_SANITIZE_MAGIC_QUOTES);

        // La date sql est au format AA/MM/JJ il faut dont que l'on l'inverse en JJ/MM/AA
        // Vérification de la syntaxe
        $date = explode('/', $date_achat);
        if (count($date) !== 3) {
            $this->error = 'Le format de la date est incorrecte. Il doit être du type AA/MM/JJ ou AAAA/MM/JJ';
            return false;
        }
        // Retournement de la date
        $date_achat = $date[2].'-'.$date[1].'-'.$date[0];

        $query = "INSERT INTO materiel_utilisateurs (id_model, id_utilisateur, localisation, date_achat) VALUES ($id_model, $id_utilisateur, '$localisation', '$date_achat')";
        if (!$this->mysqli->query($query)){
            $this->error = 'Une erreur est survenue lors de la requête suivante : '.$query.'<br>'.$this->mysqli->error;
            return false;
        }

        return true;

    }



    /**
     * Getter pour récupérer la dernière erreur enregistrée
     *
     * @function getError
     * @return string
     *
     *
     */
    public function getError () { return $this->error; }



    /**
     * dbAccess constructor.
     */
    public function __construct()
    {
        // Connexion à la base de données
        $this->mysqli = new mysqli($this->host, $this->username, $this->password, $this->dbName);

        if ($this->mysqli->connect_errno)
            echo "Impossible de se connecter au serveur MySQL: " . $this->mysqli->connect_error;

        $this->mysqli->query("SET NAMES 'utf8'");

    }


    /**
     * Destructeur
     */
    public function __destruct()
    {
        $this->mysqli->close();
    }
}