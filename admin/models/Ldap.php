<?php
/**
 * Created by PhpStorm.
 * User: cjaming
 * Date: 27/04/2018
 * Time: 09:03
 */

class Ldap
{

    // Variables
    protected $server       = '172.17.50.101';                                                // Adresse du serveur AD à contacter
    protected $ldapAdmin    = 'Administrateur@cac.local';                                        // Identifiant de l'admin ldap
    protected $ldapPassword = 'Adm.2018';                                                      // Mot de pass de l'Administrateur ldap
    protected $conState     = true;                                                             // État de la connexion
    protected $dcConnect;                                                                       // Lien de connexion vers d'AD (ldap_connect())
    protected $bind;                                                                            // Bind de la connexion (identification auprès de l'ad)

    // Emplacements dn dans l'AD
    protected $baseDn       = 'dc=cac, dc=local';                                                  // Dn de base du domaine
    protected $servicesDn   = 'ou=Utilisateurs, ou=Site Strasbourg, ou=AGDLP, ';                  // Emplacement des OU services sans dc=...
    protected $usersDn      = 'ou=*, ou=Utilisateurs, ou=Site Strasbourg, ou=AGDLP, ';            // Emplacement des utilisateurs sans dc=...
    protected $siteAdminDn  = 'ou=Informatique, ou=Utilisateurs, ou=Site Strasbourg, ou=AGDLP, '; // Emplacement des Admins du site sans dc=...
    // Getters
    public function getConState () { return $this->conState; }

    // Méthodes

    /**
     * Fonction d'authentification auprès de l'AD
     *   Si aucun paramètre n'est passé, l'authentification se fait avec l'administrateur $ldapAdmin
     * @param string $username
     * @param string $password
     * @return bool
     */
    public function bind($username='', $password='') {
        // Sans paramêtres, on se connecte en tant qu'admin
        if (empty($username) && empty($password)) {
            $username = $this->ldapAdmin;
            $password = $this->ldapPassword;
        }

        // On tente une authentification
        $this->bind = ldap_bind($this->dcConnect, $username, $password);

        // On vérifie le résultat
        if ($this->bind)
            return true;
        else
            return false;
    }

    /**
     * Fonction de récupération d'entités
     * @param $filter = users | services | siteAdmin
     * @param $where
     * @return bool|array
     */
    public function getItems ($filter, $where ) {
        if ($where === 'users')
            $dn = $this->usersDn.$this->baseDn;
        elseif ($where === 'services')
            $dn = $this->servicesDn.$this->baseDn;
        elseif ($where === 'siteAdmin')
            $dn = $this->siteAdminDn.$this->baseDn;
        else
            return false;

        var_dump($dn);

        $result = ldap_search($this->dcConnect, $dn, $filter);

        if ($result)
            return ldap_get_entries($this->dcConnect, $result);
        else
            return false;

    }

    public function getUsersInServices () {
        // Déclaration du tableau de services / utilisateurs
        $array = array();

        // On récupère toutes la liste des services
        $dn = $this->servicesDn.$this->baseDn;
        $result = ldap_search($this->dcConnect, $dn, '(ou=*)');
        if ($result)
            $rawData =  ldap_get_entries($this->dcConnect, $result);
        else
            return false;

        // Remplissage du tableau avec les services
        foreach ($rawData as $item) {
            // On ignore l'ou Utilisateurs
            if ($item['ou'][0] === 'Utilisateurs' || empty($item['ou'][0]))
                continue;

            // On récupère le nom des service ainsi que leur dn
            $array[$item['ou'][0]] = array();
            $array[$item['ou'][0]]['dn'] = $item['distinguishedname'][0];
            //$array[$item['ou'][0]] = array();
            //$array[$item['ou'][0]]['dn'] = $item['ou']['distinguishedname'];
        }

        // On récupère ensuite les utilisateurs
        foreach ($array as $item => $value) {
            $array[$item]['utilisateurs'] = array();
            $dn = $value['dn'];
            // On récupère toutes la des utilisateurs du service
            $result = ldap_search($this->dcConnect, $dn, '(cn=*)');
            if ($result)
                $rawData = ldap_get_entries($this->dcConnect, $result);
            else
                return false;

            foreach ($rawData as $item2) {
                // On ignore les CN vides
                if ($item2['samaccountname'][0] == '')
                    continue;
                //var_dump($item2);
                //echo '<br /><br />Utilisateur: '.$item2['samaccountname'][0].'<br />';
                $array[$item]['utilisateurs'][$item2['samaccountname'][0]] = $item2['distinguishedname'][0];

            }
        }

        return $array;


    }
    /**
array(26) {
        ["objectclass"]=> array(3) {
            ["count"]=> int(2) [0]=> string(3) "top" [1]=> string(18) "organizationalUnit"
        }
        [0]=> string(11) "objectclass" ["ou"]=> array(2) {
            ["count"]=> int(1) [0]=> string(12) "Utilisateurs"
        }
        [1]=> string(2) "ou" ["distinguishedname"]=> array(2) {
            ["count"]=> int(1) [0]=> string(58) "OU=Utilisateurs,OU=Site Strasbourg,OU=AGDLP,DC=cac,DC=local"
        }
        [2]=> string(17) "distinguishedname" ["instancetype"]=> array(2) {
            ["count"]=> int(1) [0]=> string(1) "4"
        }
        [3]=> string(12) "instancetype" ["whencreated"]=> array(2) {
            ["count"]=> int(1) [0]=> string(17) "20180426105340.0Z"
        }
        [4]=> string(11) "whencreated" ["whenchanged"]=> array(2) {
            ["count"]=> int(1) [0]=> string(17) "20180426105354.0Z"
        }
        [5]=> string(11) "whenchanged" ["usncreated"]=> array(2) {
            ["count"]=> int(1) [0]=> string(5) "12885"
        }
        [6]=> string(10) "usncreated" ["usnchanged"]=> array(2) {
            ["count"]=> int(1) [0]=> string(5) "12889"
        }
        [7]=> string(10) "usnchanged" ["name"]=> array(2) {
            ["count"]=> int(1) [0]=> string(12) "Utilisateurs"
        }
        [8]=> string(4) "name" ["objectguid"]=> array(2) {
            ["count"]=> int(1) [0]=> string(16) "5�I�K�A�G�*�:a"
        }
        [9]=> string(10) "objectguid" ["objectcategory"]=> array(2) {
            ["count"]=> int(1) [0]=> string(64) "CN=Organizational-Unit,CN=Schema,CN=Configuration,DC=cac,DC=local"
        }
        [10]=> string(14) "objectcategory" ["dscorepropagationdata"]=> array(5) {
            ["count"]=> int(4) [0]=> string(17) "20180426105354.0Z" [1]=> string(17) "20180426105340.0Z" [2]=> string(17) "20180426105340.0Z" [3]=> string(17) "16010101000000.0Z"
        }
        [11]=> string(21) "dscorepropagationdata" ["count"]=> int(12) ["dn"]=> string(58) "OU=Utilisateurs,OU=Site Strasbourg,OU=AGDLP,DC=cac,DC=local" }
**/

    /**
     * Ldap constructor.
     */
    public function __construct()
    {
        $this->dcConnect = ldap_connect($this->server);
        if (!$this->dcConnect)
            $this->conState = false;
        else
            $this->conState = true;
    }

    /**
     * Ldap destructor
     */
    public function __destruct()
    {
        ldap_close($this->dcConnect);
    }
}
