<?php
/**
 * Created by PhpStorm.
 * User: cjaming
 * Date: 26/04/2018
 * Time: 13:49
 */

session_start();

$userDn     = 'cn=Users, dc=cac, dc=local';  // dn des utilisateurs autres
$adminDn    = 'ou=Informatique, ou=Utilisateurs, ou=Site Strasbourg, ou=AGDLP, dc=cac, dc=local'; // dn des utilisateurs du service informatique pour l'authentification.
$DcHostname = '172.17.50.101';

if (empty($_SESSION['username']) && !empty($_POST) && !empty($_POST['login']) && !empty($_POST['password'])) {

    // Les 2 lignes ci-dessous servent à gruger l'authentification pour travailler en local
    // il faudra décommenter/decommenter les lignes plus bas et supprimer ces 2 pour remettre l'authentification ad en place
    //$_SESSION['username'] = $_POST['login'];
    //header('location: /admin/index.php');

// Connexion au serveur
    $dc = ldap_connect($DcHostname);
    if ($dc) {
        // On vérifie le couple login/pass
        $bind = ldap_bind($dc, $_POST['login'] . '@cac.local', $_POST['password']);
        if ($bind) {
            // On vérifie si la personne est authorisée à accéder au site (user dans l'OU Informatique)
            $bind = ldap_bind($dc, 'Administrateur@cac.local', 'Adm.2018');
            if ($bind) {
                $res = ldap_search($dc, $adminDn, '(samaccountname='.$_POST['login'].')');
                if (ldap_count_entries($dc, $res) != 0) {
                    $_SESSION['username'] = $_POST['login'];
                    header('location: /admin/index.php');
                }
                else {
                    $_SESSION['message'] = 'Vous n\'êtes pas autorisé à accéder à ce site';
                }
            }
            else {
                $_SESSION['message'] = "Vérifiez les options de connexions en tant qu'admin.";
            }
        } else {
            $_SESSION['message'] = 'Erreur d\'authentification.';
        }

// Fermeture de la connexion ldap
        ldap_close($dc);
    }

    else {
        $_SESSION['message'] = 'Erreur: Impossible de se connecter au serveur '. $DcHostname;
    }


}
?>
