<?php
/**
 * Created by PhpStorm.
 * User: cjaming
 * Date: 23/04/2018
 * Time: 16:43
 */

session_start();

if (isset($_SESSION) && isset($_SESSION['username']))
    header('location: /admin/index.php');

require_once ($_SERVER['DOCUMENT_ROOT'].'/auth.inc.php');

?>


    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="/css/bootstrap.css">
        <title>Outil de gestion de parc</title>
    </head>
<body>

<div style="height: 100%;">
    <div class="container" style="width: 30%; padding-top: 50px;">
        <div style="text-align: center">
            <img style="max-width: 70%;" src="/images/logo.png">
        </div>

        <legend style="text-align: center">Outil de gestion de parc</legend>
        <?php
        // Affichage le message si il existe
        if(!empty($_SESSION['message'])) {
            echo '<p style="color: red; font-weight: bold">'.$_SESSION['message'].'</p>';
            $_SESSION['message'] = '';
            unset($_SESSION);
        }
        ?>

        <form action="/index.php" method="post" style="width: auto; margin-left: auto; margin-right: auto">
            <div class="form-group">
                <label for="login" class="col-form-label">Login</label>
                <input type="text" class="form-control" id="login" name="login" placeholder="Login">
            </div>
            <div class="form-group">
                <label for="password" class="col-form-label">Mot de passe</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="*****************">
            </div>
            <button type="submit" class="btn btn-info btn-lg btn-block" name="send" value="Connexion">Connexion</button>

        </form>
    </div>
</div>

<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.js"></script>
</body>
</html>

