// Fonction d'affichage du matériel en fonction du type choisi.
function afficherTableauMateriel() {

    // Affichage des produits disponibles dans ce dépôt
    $type = $('select#type').val();
    $utilisateur = $('select#utilisateurs').val();

    $.post('/admin/ajax/afficherTableauMateriel.php', {type: $type, utilisateur: $utilisateur}, function (data) {
        $('div#divTableauMateriel').html(data);
    });

}

// Fonction d'affichage des models disponibles en fonction du type choisi.
function afficherTableauModels() {

    // Affichage des produits disponibles dans ce dépôt

    $type = $('select#type').val();
    $marque = $('select#marque').val();

    console.log($type + $marque);
    $.post('/admin/ajax/afficherTableauModels.php', {type: $type, marque: $marque}, function (data) {
        $('div#divTableauModels').html(data);
    });

}


// Fonction d'affichage des utilisateurs en fonction des services.
function afficherTableauUtilisateurs() {

    // Affichage des produits disponibles dans ce dépôt
    $service = $('select#id_service').val();

    $.post('/admin/ajax/afficherTableauUtilisateurs.php', {service: $service}, function (data) {
        $('div#divTableauUtilisateurs').html(data);
    });

}



// Analyse du document
$(document).ready( function () {

    $('button.btn-ajoutParc').click(function () {
        $id = $(this).parent().attr('id');
        $type = $(this).parent().prev().html();
        $marque = $(this).parent().prev().prev().html();
        $model = $(this).parent().prev().prev().prev().html();

        $objet = $type + ' ' + $marque + ' ' + $model;
        console.log($type);
        console.log($marque);
        console.log($model);
        console.log($objet);

        $('label#descriptionModel').html($objet);
        $('input#id_model').val($id);
    });

});