<?php
/**
 * Created by PhpStorm.
 * User: cjaming
 * Date: 26/04/2018
 * Time: 13:54
 */

// Démarrage ou restauration de la session
session_start();
// Réinitialisation du tableau de session
// On le vide intégralement
$_SESSION = array();
// Destruction de la session
session_destroy();
// Destruction du tableau de session
unset($_SESSION);

header('location: /');